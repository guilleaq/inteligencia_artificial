# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    # Initialize the stack for DFS. Nodes are processed in a Last-In-First-Out (LIFO) manner.
    stack = util.Stack()

    # Initialize the start node and path.
    startNode = problem.getStartState()
    path = []

    # Push the start node to the stack.
    stack.push((startNode, path))

    # Set to keep track of expanded nodes.
    expanded = set()

    while not stack.isEmpty():
        # Pop the node from the top of the stack.
        node, path = stack.pop()

        # If this node is the goal, return the path leading to it.
        if problem.isGoalState(node):
            return path
        
        # If the node has not been expanded yet.
        if node not in expanded:
            expanded.add(node)
            
            # Iterate over the node's successors.
            for successor, action, cost in problem.getSuccessors(node):
                # Create a new path that includes the action leading to the successor.
                new_path = path + [action]

                # Push the successor to the top of the stack for further exploration.
                stack.push((successor, new_path))

    # If the stack is empty and no solution is found, return None.
    return

def breadthFirstSearch(problem: SearchProblem):
    # Initialize the queue for BFS. Nodes are processed in a First-In-First-Out (FIFO) manner.
    queue = util.Queue()

    # Initialize the start node and path.
    startNode = problem.getStartState()
    path = []

    # Push the start node to the queue.
    queue.push((startNode, path))

    # Set to keep track of expanded nodes.
    expanded = set()

    while not queue.isEmpty():
        # Pop the node from the front of the queue.
        node, path = queue.pop()

        # If this node is the goal, return the path leading to it.
        if problem.isGoalState(node):
            return path
        
        # If the node has not been expanded yet.
        if node not in expanded:
            expanded.add(node)
            
            # Iterate over the node's successors.
            for successor, action, cost in problem.getSuccessors(node):
                # Create a new path that includes the action leading to the successor.
                new_path = path + [action]

                # Push the successor to the back of the queue for further exploration.
                queue.push((successor, new_path))

    # If the queue is empty and no solution is found, return None.
    return

def uniformCostSearch(problem: SearchProblem):
    # Initialize the priority queue where nodes are prioritized based on their cumulative cost
    priorityQueue = util.PriorityQueue()

    # Initialize start node, path, and the initial cost (which should be 0 at the start)
    startNode = problem.getStartState()
    path = []
    cost = problem.getCostOfActions(path)
    
    # Push the start node to the priority queue with its associated cost
    priorityQueue.push((startNode, path), cost)
    
    # Set to keep track of expanded nodes
    expanded = set()

    while not priorityQueue.isEmpty():
        # Pop the node with the lowest cumulative cost
        node, path = priorityQueue.pop()

        # If this node is the goal, return the path leading to it
        if problem.isGoalState(node):
            return path
        
        # If the node has not been expanded yet
        if node not in expanded:
            expanded.add(node)
            
            # Iterate over the node's successors
            for successor, action, cost in problem.getSuccessors(node):
                # Create a new path including the action leading to the successor
                new_path = path + [action]
                # Calculate the total cost of the new path
                cost = problem.getCostOfActions(new_path)
                
                # Push the successor to the priority queue with its associated cost
                priorityQueue.push((successor, new_path), cost)

    # If the priority queue is empty and no solution is found, return None
    return

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem: SearchProblem, heuristic=nullHeuristic):
    def calculate_heuristic(node, heuristic):
        # Cost from start node to current node (g)
        cost = problem.getCostOfActions(node[1])
        # f = g + h
        priority = cost + heuristic(node[0], problem)
        return priority

    # Initialize start node, path, and the priority queue
    startNode = problem.getStartState()
    path = []
    priorityQueueWithFunction = util.PriorityQueueWithFunction(calculate_heuristic)
    priorityQueueWithFunction.push((startNode, path), heuristic)
    expanded = set()  # To keep track of expanded nodes

    while not priorityQueueWithFunction.isEmpty():
        # Pop the node with the lowest heuristic value
        node, path = priorityQueueWithFunction.pop()

        # If this node is the goal, return the path leading to it
        if problem.isGoalState(node):
            return path
        
        # If the node has not been expanded yet
        if node not in expanded:
            expanded.add(node)
            # Iterate over the node's successors
            for successor, action, cost in problem.getSuccessors(node):
                # Create a new path including the action leading to the successor
                new_path = path + [action]
                # Push the successor to the priority queue for further exploration
                priorityQueueWithFunction.push((successor, new_path), heuristic)

    # If the priority queue is empty and no solution is found, return None
    return

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
